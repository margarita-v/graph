﻿using System.Collections.Generic;

namespace Graph_NEW_VERSION
{
    public interface IList<T> : IEnumerable<T>
    {
        void Add(T value);
        void Clear();
        bool Contains(T value);
        int IndexOf(T value);
        void Insert(int index, T value);
        void Remove(T value);
        void RemoveAt(int index);
        int Count { get; set; }
        T this[int index] { get; set; }
    }
}
