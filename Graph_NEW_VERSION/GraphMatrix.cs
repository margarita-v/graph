﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Graph_NEW_VERSION
{
    public class GraphMatrix<V, E> : IGraph<V, E>
        where E : IComparable
        where V : IComparable
    {
        int size;
        E[,] matrix;
        V[] nodes;

        public GraphMatrix()
        {
            size = 0;
            matrix = new E[0, 0];
            nodes = new V[0];
        }

        public E[,] Matrix
        {
            get { return matrix; }
        }
        public V[] Nodes
        {
            get { return nodes; }
        }

        E[,] Increase()
        {
            int newSize = size + 1;
            E[,] result = new E[newSize, newSize];

            // копируем предыдущую матрицу 
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    result[i, j] = matrix[i, j];
                }
            }

            // обнуляем новые значения
            for (int i = 0; i < size; i++)
            {
                result[i, size] = default(E);
                result[size, i] = default(E);
            }
            result[size, size] = default(E);

            V[] newNodes = new V[newSize];
            for (int i = 0; i < size; i++)
                newNodes[i] = nodes[i];

            nodes = newNodes;
            size++;
            return result;
        }

        public int FindNode(V info)
        {
            for (int i = 0; i < size; i++)
                if (nodes[i].Equals(info))
                    return i;
            return -1;
        }
        public void FindEdge(V first, V second, out int i, out int j)
        {
            i = -1;
            j = -1;
            for (int ind = 0; ind < size; ind++)
                for (int knd = 0; knd < size; knd++)
                    if (nodes[ind].Equals(first) && nodes[knd].Equals(second) && !matrix[ind, knd].Equals(default(E)))// ||
                       // nodes[ind].Equals(second) && nodes[knd].Equals(first) && !matrix[ind, knd].Equals(default(E)))
                    {
                        i = ind;
                        j = knd;
                    }
        }

        // РЕАЛИЗАЦИЯ МЕТОДОВ ИНТЕРФЕЙСА

        public void AddNode(V info)
        {
            if (ContainsNode(info))
                throw new GraphException("Вершина " + info + " уже есть в графе.");
            matrix = Increase();
            nodes[size - 1] = info;
        }
        public void AddEdge(V StartNode, V EndNode, E weight)
        {
            if (StartNode.Equals(default(V)) || EndNode.Equals(default(V)))
                throw new GraphException("Значения начальной и конечной вершин ребра не должны быть равны " + default(V));

            int i, j;
            FindEdge(StartNode, EndNode, out i, out j);
            if (i != -1)
                throw new GraphException("Такое ребро уже есть в графе.");

            i = FindNode(StartNode);
            j = FindNode(EndNode);
            if (i == -1 || j == -1)
                throw new GraphException("Cannot add an arc if one of its extremity nodes does not belong to the graph.");

            matrix[i, j] = weight;
            matrix[j, i] = weight;
        }

        public void Clear()
        {
            size = 0;
            matrix = new E[0, 0];
            nodes = new V[0];
        }

        public bool ContainsNode(V info)
        {
            for (int i = 0; i < size; i++)
                if (nodes[i].Equals(info))
                    return true;
            return false;
        }
        public bool ContainsEdge(V StartNode, V EndNode)
        {
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    if (nodes[i].Equals(StartNode) && nodes[j].Equals(EndNode) && !matrix[i, j].Equals(default(E)) ||
                        nodes[i].Equals(EndNode) && nodes[j].Equals(StartNode) && !matrix[i, j].Equals(default(E)))
                        return true;
            return false;
        }

        public void DeleteSingleNodes()
        {
            for (int i = 0; i < size; i++) 
            {
                int count = 0;
                for (int j = 0; j < size; j++)
                    if (!matrix[i, j].Equals(default(E)))
                        count++;
                if (count == 0)
                    RemoveNode(nodes[i]);
            }
        }
        public void RemoveNode(V info)
        {
            if (!ContainsNode(info))
                throw new GraphException("Данная вершина отсутствует в графе.");
            int i = FindNode(info);

            int newSize = size - 1;
            E[,] newMatrix = new E[newSize, newSize];

            // удаляем нужные строку и столбец
            for (int ind = i; ind < newSize; ind++)
            {
                for (int j = 0; j < newSize; j++)
                {
                    matrix[ind, j] = matrix[ind + 1, j];
                    matrix[j, ind] = matrix[j, ind + 1];
                }
            }
            matrix[newSize - 1, newSize - 1] = matrix[size - 1, size - 1];

            // переписываем все в новую матрицу
            for (int ind = 0; ind < size - 1; ind++)
                for (int j = 0; j < size - 1; j++)
                    newMatrix[ind, j] = matrix[ind, j];

            newMatrix[newSize - 1, newSize - 1] = matrix[newSize - 1, newSize - 1];
            matrix = newMatrix;

            V[] newNodes = new V[newSize];

            //удаляем из массива узлов нужный узел
            for (int ind = i; ind < newSize; ind++)
                nodes[ind] = nodes[ind + 1];

            // Ппереписываем все в новый список узлов
            for (int ind = 0; ind < size - 1; ind++)
                newNodes[ind] = nodes[ind];

            nodes = newNodes;
            size--;
        }
        public void RemoveEdge(V StartNode, V EndNode)
        {
            if (!ContainsEdge(StartNode, EndNode))
                throw new GraphException("Ребро между данными вершинами отсутствует в графе.");

            int i, j;
            FindEdge(StartNode, EndNode, out i, out j);
            matrix[i, j] = default(E);
            DeleteSingleNodes();
        }

        public int CountOfNodes
        {
            get { return size; }
        }
        public int CountOfEdges
        {
            get 
            {
                int count = 0;
                for (int i = 0; i < size; i++)
                    for (int j = i; j < size; j++)
                        if (!matrix[i, j].Equals(default(E)))
                            count++;
                return count;
            }
        }

        public Elements<V> GetListOfNodes
        {
            get
            {
                Elements<V> res = new Elements<V>();
                for (int i = 0; i < size; i++)
                    res.Add(nodes[i]);
                return res;
            }
        }

        public IEnumerator<V> GetEnumerator()
        {
            for (int i = 0; i < size; i++)
                yield return nodes[i];
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        private string items = "";
        bool[] marks;
        void DepthTraversal(int index)
        {
            marks[index] = true;
            items = items + nodes[index] + '\n';

            for (int i = 0; i < size; i++)
                if (!matrix[i, index].Equals(default(E)) && !marks[i])
                    DepthTraversal(i);
        }
        public void DoDepthTraversal(out string res)
        {
            if (CountOfEdges > 0 && CountOfNodes > 0)
            {
                marks = new bool[size];
                for (int i = 0; i < size; i++)
                    marks[i] = false;

                DepthTraversal(0);
                int count = 0;
                for (int i = 0; i < size; i++)
                {
                    if (marks[i])
                        count++;
                    marks[i] = false;
                }
                res = items;
                items = "";
                if (count < CountOfNodes)
                    throw new GraphException("Граф не является связным.");
            }
            else
                throw new GraphException("Граф пуст.");
        }

        void BreadthTraversal(int index)
        {
            int[] q = new int[CountOfNodes + 1];
            int p_write = 1, p_read = 0;
            q[p_write] = index;
            marks[index] = true;
            while (p_read < p_write)
            {
                p_read++;
                index = q[p_read];
                items = items + nodes[index] + '\n';
                for (int i = 0; i < size; i++)
                    if (!matrix[i, index].Equals(default(E)) && !marks[i])
                    {
                        p_write++;
                        q[p_write] = i;
                        marks[i] = true;
                    }
            }
        }
        public void DoBreadthTraversal(out string res)
        {
            if (CountOfEdges > 0 && CountOfNodes > 0)
            {
                marks = new bool[size];
                for (int i = 0; i < size; i++)
                    marks[i] = false;

                BreadthTraversal(0);
                int count = 0;
                for (int i = 0; i < size; i++)
                {
                    if (marks[i])
                        count++;
                    marks[i] = false;
                }
                res = items;
                items = "";
                if (count < CountOfNodes)
                    throw new GraphException("Граф не является связным.");
            }
            else
                throw new GraphException("Граф пуст.");
        }

        Elements<string> ways;
        Elements<int> visited;
        string way = "";
        void TryGo(int node, int end, Elements<int> visited)
        {
            if (node == end)
                ways.Add(way);
            else
                for (int i = 0; i < size; i++)
                    if (!matrix[i, node].Equals(default(E)) && !visited.Contains(i))
                    {
                        string w = way;
                        way = way + matrix[i, node] + ' ';
                        visited.Add(i);
                        TryGo(i, end, visited);
                        visited.Remove(i);
                        way = w;
                    }
        }

        void TryGo2(int node, int end, Elements<int> visited)
        {
            if (node == end)
                ways.Add(way);
            else
                for (int i = 0; i < size; i++)
                    if (!matrix[i, node].Equals(default(E)) && !visited.Contains(i))
                    {
                        string w = way;
                        way = way + nodes[i] + ' ';
                        visited.Add(i);
                        TryGo2(i, end, visited);
                        visited.Remove(i);
                        way = w;
                    }
        }

        public void MinPath(V first, V second, out string res, out int weight)
        {
            ways = new Elements<string>();
            visited = new Elements<int>();
            res = "";
            int min_sum = Int32.MaxValue;
            weight = min_sum;
            int _first = FindNode(first);
            int _second = FindNode(second);

            visited.Add(_first);
            TryGo(_first, _second, visited);
            foreach (string s in ways)
            {
                string[] m = s.Split(' ');
                int[] mas = new int[m.Length - 1];
                int sum = 0;
                for (int i = 0; i < m.Length - 1; i++)
                {
                    mas[i] = Int32.Parse(m[i]);
                    sum += mas[i];
                }
                if (sum < min_sum)
                {
                    min_sum = sum;
                    weight = min_sum;
                    res = s;
                }
                //res = res + s + '\n';
            }
            ways.Clear();
            visited.Clear();
            way = "";
        }

        public void AllPathes(V first, V second, out string res)
        {
            ways = new Elements<string>();
            visited = new Elements<int>();
            res = "";
            int _first = FindNode(first);
            int _second = FindNode(second);

            visited.Add(_first);
            TryGo2(_first, _second, visited);

            foreach (string s in ways)
                res = res + first + ' ' + s + '\n';
            ways.Clear();
            visited.Clear();
            way = "";
        }

        public void GetMatrix(DataGridView grid)
        {
            grid.Rows.Clear();
            if (size > 0)
            {
                grid.RowCount = size + 1;
                grid.ColumnCount = grid.RowCount;
                int i = 1;
                for (int ind = 0; ind < size; ind++, i++)
                {
                    grid.Rows[0].Cells[i].Value = nodes[ind].ToString();
                    grid.Rows[i].Cells[0].Value = nodes[ind].ToString();
                    for (int k = 0; k < size; k++)
                        if (!matrix[ind, k].Equals(default(E)))
                        {
                            grid.Rows[ind + 1].Cells[k + 1].Value = matrix[ind, k];
                            grid.Rows[k + 1].Cells[ind + 1].Value = matrix[ind, k];
                        }
                }
            }
        }

        public void DoAddition(V first, V second, E weight, DataGridView grid)
        {
            int first_node = FindNode(first);
            int second_node = FindNode(second);
            if (first_node == -1)
            {
                AddNode(first);
                if (second_node == -1)
                    AddNode(second);
            }
            else
            {
                if (second_node == -1)
                    AddNode(second);
            }
            AddEdge(first, second, weight);
            GetMatrix(grid);
        }
    }
}
