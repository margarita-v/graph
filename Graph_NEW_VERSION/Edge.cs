﻿namespace Graph_NEW_VERSION
{
    public class Edge<V, E>
    {
        Node<V, E> startNode, endNode;  // начальная и конечная вершины ребра
        E weight;                       // вес ребра

        public Edge(Node<V, E> Start, Node<V, E> End)
        {
            StartNode = Start;
            EndNode = End;
            Weight = default(E);
        }
        public Edge(Node<V, E> Start, Node<V, E> End, E weight)
        {
            StartNode = Start;
            EndNode = End;
            Weight = weight;
        }

        public Node<V, E> StartNode
        {
            set
            {
                if (value == null) throw new GraphException("StartNode");
                if (EndNode != null && value.Equals(EndNode)) throw new GraphException("StartNode and EndNode must be different");
                startNode = value;
            }
            get { return startNode; }
        }

        public Node<V, E> EndNode
        {
            set
            {
                if (value == null) throw new GraphException("EndNode");
                if (StartNode != null && value.Equals(StartNode)) throw new GraphException("StartNode and EndNode must be different");
                endNode = value;
            }
            get { return endNode; }
        }

        public E Weight
        {
            set { weight = value; }
            get { return weight; }
        }

        public override string ToString()
        {
            return startNode.ToString() + "---" + endNode.ToString();
        }

        public override bool Equals(object O)
        {
            Edge<V, E> E = (Edge<V, E>)O;
            if (E == null) throw new GraphException("Cannot compare type " + GetType() + " with type " + O.GetType() + " !");
            return startNode.Equals(E.StartNode) && endNode.Equals(E.EndNode) || startNode.Equals(E.EndNode) && endNode.Equals(E.StartNode);
        }

        public override int GetHashCode() { return Weight.GetHashCode(); }
    }
}
