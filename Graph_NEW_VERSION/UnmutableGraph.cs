﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Graph_NEW_VERSION
{
    public class UnmutableGraph<V, E> : IGraph<V, E>
        where E : IComparable
        where V : IComparable
    {
        private readonly IGraph<V, E> graph;

        public UnmutableGraph(IGraph<V, E> Graph)
        {
            graph = Graph;
        }

        // РЕАЛИЗАЦИЯ МЕТОДОВ ИНТЕРФЕЙСА

        public void AddNode(V info)
        {
            throw new GraphException("Попытка добавления вершины в неизменяемый граф.");
        }
        public void AddEdge(V StartNode, V EndNode, E weight)
        {
            throw new GraphException("Попытка добавления ребра в неизменяемый граф.");
        }

        public void Clear()
        {
            throw new GraphException("Попытка очистки неизменяемого графа.");
        }

        public bool ContainsNode(V info) 
        {
            return graph.ContainsNode(info);
        }
        public bool ContainsEdge(V StartNode, V EndNode)
        {
            return graph.ContainsEdge(StartNode, EndNode);
        }

        public void DeleteSingleNodes()
        {
            throw new GraphException("Попытка удалени вершин из неизменяемого графа.");
        }
        public void RemoveNode(V info)
        {
            throw new GraphException("Попытка удаления вершины из неизменяемого графа.");
        }
        public void RemoveEdge(V StartNode, V EndNode)
        {
            throw new GraphException("Попытка удаления ребра из неизменяемого графа.");
        }

        public int CountOfNodes 
        {
            get { return graph.CountOfNodes; }
        }
        public int CountOfEdges 
        {
            get { return graph.CountOfEdges; }
        }

        public Elements<V> GetListOfNodes
        {
            get { return graph.GetListOfNodes; }
        }

        public IEnumerator<V> GetEnumerator()
        {
            return graph.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public void DoDepthTraversal(out string res)
        {
            graph.DoDepthTraversal(out res);
        }
        public void DoBreadthTraversal(out string res)
        {
            graph.DoBreadthTraversal(out res);
        }

        public void MinPath(V first, V second, out string res, out int weight)
        {
            graph.MinPath(first, second, out res, out weight);
        }
        public void AllPathes(V first, V second, out string res)
        {
            graph.AllPathes(first, second, out res);
        }

        public void GetMatrix(DataGridView grid)
        {
            graph.GetMatrix(grid);
        }

        public void DoAddition(V first, V second, E weight, DataGridView grid)
        {
            throw new GraphException("Граф изменить нельзя.");
        }
    }
}
