﻿using System.Collections;
using System.Collections.Generic;

namespace Graph_NEW_VERSION
{
    public class Elements<T> : IList<T>, IEnumerable<T>
    {
        protected _Node<T> head;
        protected _Node<T> current = null;
        int count;

        protected class _Node<T>
        {
            private _Node<T> next;
            private T data;
            public _Node(T t)
            {
                next = null;
                data = t;
            }
            public _Node<T> Next
            {
                get { return next; }
                set { next = value; }
            }
            public T Data
            {
                get { return data; }
                set { data = value; }
            }
        }

        public Elements()
        {
            head = null;
            count = 0;
        }

        // РЕАЛИЗАЦИЯ МЕТОДОВ ИНТЕРФЕЙСА
        public void Add(T value)
        {
            _Node<T> cur = head;
            _Node<T> newNode = new _Node<T>(value);
            if (cur == null)
                head = newNode;
            else
            {
                while (cur.Next != null)
                    cur = cur.Next;
                cur.Next = newNode;
            }
            count++;
        }
        public void Clear()
        {
            for (int i = count - 1; i >= 0; i--)
                RemoveAt(i);
            count = 0;
        }
        public bool Contains(T value)
        {
            bool found = false;
            _Node<T> cur = head;
            while (cur != null && !found)
            {
                found = cur.Data.Equals(value);
                cur = cur.Next;
            }
            return found;
        }
        public int IndexOf(T value)
        {
            int index = -1;
            _Node<T> cur = head;
            for (int i = 0; i < count && cur != null; i++, cur = cur.Next)
                if (cur.Data.Equals(value))
                {
                    index = i;
                    break;
                }
            return index;
        }
        public void Insert(int index, T value)
        {
            if ((index < count) && (index >= 0))
            {
                _Node<T> cur = head;
                for (int i = 0; i < index; i++, cur = cur.Next) ;
                _Node<T> newNode = new _Node<T>(value);
                newNode.Next = cur.Next;
                cur.Next = newNode;
                count++;
            }
        }
        public void Remove(T value)
        {
            RemoveAt(IndexOf(value));
        }
        public void RemoveAt(int index)
        {
            if ((index >= 0) && (index < count))
            {
                if (index != 0)
                {
                    _Node<T> cur = head;
                    for (int i = 0; i < index - 1; i++, cur = cur.Next) ;
                    cur.Next = cur.Next.Next;
                }
                else
                    head = head.Next;
                count--;
            }

        }
        public int Count
        {
            get { return count; }
            set { count = value; }
        }
        public T this[int index]
        {
            get
            {
                _Node<T> cur = head;
                for (int i = 0; i < index && cur != null; i++, cur = cur.Next) ;
                if (cur == null)
                    throw new GraphException("Invalid index.");
                return cur.Data;
            }
            set
            {
                _Node<T> cur = head;
                for (int i = 0; i < index && cur != null; i++, cur = cur.Next) ;
                if (cur == null)
                    throw new GraphException("Invalid index.");
                cur.Data = value;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            _Node<T> cur = head;
            while (cur != null)
            {
                yield return cur.Data;
                cur = cur.Next;
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
