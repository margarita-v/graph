﻿using System;
using System.Windows.Forms;

namespace Graph_NEW_VERSION
{
    public partial class FormMain : Form
    {
        IGraph<int, int> IG = null;
        IGraph<string, int> SG = null;

        public FormMain()
        {
            InitializeComponent();
        }

        private void addEdge_Click(object sender, EventArgs e)
        {
            FormInputQuery frm = new FormInputQuery("Введите значения вершин, между которым нужно провести ребро.", true);
            frm.ShowDialog();
            int i_first = 0, i_second = 0;
            string first = "", second = "";
            int weight = 0;
            if (frm.ok)
            {
                try
                {
                    if (rbInt.Checked)
                    {
                        if (!(Int32.TryParse(frm.tbFirst.Text, out i_first) && Int32.TryParse(frm.tbSecond.Text, out i_second) && Int32.TryParse(frm.tbWeight.Text, out weight)))
                            throw new GraphException("Неверный тип элемента. Вершины графа имеют тип " + i_first.GetType().ToString() + "; \nВеса ребер графа имеют тип " + weight.GetType().ToString());
                        
                        if (cbUnmutable.Checked) // int unmutable
                        {
                            IG = new UnmutableGraph<int, int>(IG);
                            IG.DoAddition(i_first, i_second, weight, Grid);
                        }
                        else // cbUnmutable.Checked = false
                        {
                            if (rbList.Checked) // int list
                            {
                                if (IG == null) 
                                    IG = new Graph<int, int>();
                                IG.DoAddition(i_first, i_second, weight, Grid);
                            }
                            else // rbMatrix.Checked // int matrix
                            {
                                if (IG == null) 
                                    IG = new GraphMatrix<int, int>();
                                IG.DoAddition(i_first, i_second, weight, Grid);
                            }
                        }
                    }
                    else // rbString.Checked
                    {
                        first = frm.tbFirst.Text;
                        second = frm.tbSecond.Text;
                        if (!(Int32.TryParse(frm.tbWeight.Text, out weight)))
                            throw new GraphException("Неверный тип элемента. Вершины графа имеют тип " + first.GetType().ToString() + "; \nВеса ребер графа имеют тип " + weight.GetType().ToString());
                        
                        if (cbUnmutable.Checked) // string unmutable
                        {
                            SG = new UnmutableGraph<string, int>(SG);
                            SG.DoAddition(first, second, weight, Grid);
                        }
                        else // cbUnmutable.Checked = false
                        {
                            if (rbList.Checked) // string list
                            {
                                if (SG == null) 
                                    SG = new Graph<string, int>();
                                SG.DoAddition(first, second, weight, Grid);
                            }
                            else // rbMatrix.Checked // string matrix
                            {
                                if (SG == null) 
                                    SG = new GraphMatrix<string, int>();
                                SG.DoAddition(first, second, weight, Grid);
                            }
                        }
                    }
                }
                catch (GraphException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    delEdge.Enabled = true;
                    clearGraph.Enabled = true;
                    depthTraversal.Enabled = true;
                    breadthTraversal.Enabled = true;
                    shortestWay.Enabled = true;
                    allPathes.Enabled = true;

                    findNode.Enabled = true;
                    countOfNodes.Enabled = true;
                    checkNodes.Enabled = true;
                    getNodesInOrder.Enabled = true;

                    cbUnmutable.Enabled = true;
                }
            }
        }

        private void delEdge_Click(object sender, EventArgs e)
        {
            FormInputQuery frm = new FormInputQuery("Введите значения вершин, между которым нужно удалить ребро.", false);
            frm.ShowDialog();
            int i_first = 0, i_second = 0;
            string first = "", second = "";
            if (frm.ok)
            {
                try
                {
                    if (rbInt.Checked)
                    {
                        if (!(Int32.TryParse(frm.tbFirst.Text, out i_first) && Int32.TryParse(frm.tbSecond.Text, out i_second)))
                            throw new GraphException("Неверный тип элемента. Вершины графа имеют тип " + i_first.GetType().ToString());

                        if (cbUnmutable.Checked) // int unmutable
                            IG.RemoveEdge(i_first, i_second);
                            
                        else // cbUnmutable.Checked = false
                        {
                            if (rbList.Checked) // int list
                                IG.RemoveEdge(i_first, i_second);
                            else // rbMatrix.Checked // int matrix
                                IG.RemoveEdge(i_first, i_second);
                        }
                        IG.GetMatrix(Grid);
                    }
                    else // rbString.Checked
                    {
                        first = frm.tbFirst.Text;
                        second = frm.tbSecond.Text;

                        if (cbUnmutable.Checked) // string unmutable
                            SG.RemoveEdge(first, second);
                        else // cbUnmutable.Checked = false
                        {
                            if (rbList.Checked) // string list
                                SG.RemoveEdge(first, second);
                            else // rbMatrix.Checked // string matrix
                                SG.RemoveEdge(first, second);
                        }
                        SG.GetMatrix(Grid);
                    }
                }
                catch (GraphException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    delEdge.Enabled = true;
                    clearGraph.Enabled = true;
                    depthTraversal.Enabled = true;
                    breadthTraversal.Enabled = true;
                    shortestWay.Enabled = true;
                    allPathes.Enabled = true;
                }
            }
        }

        private void shortestWay_Click(object sender, EventArgs e)
        {
            FormInputQuery frm = new FormInputQuery("Введите значения вершин.", false);
            frm.ShowDialog();
            int i_first = 0, i_second = 0;
            string first = "", second = "", result = "";
            int weight = 0;
            if (frm.ok)
            {
                try
                {
                    if (rbInt.Checked)
                    {
                        if (!(Int32.TryParse(frm.tbFirst.Text, out i_first) && Int32.TryParse(frm.tbSecond.Text, out i_second)))
                            throw new GraphException("Неверный тип элемента. Вершины графа имеют тип " + i_first.GetType().ToString());

                        if (cbUnmutable.Checked) // int unmutable
                            IG.MinPath(i_first, i_second, out result, out weight);
                        else // cbUnmutable.Checked = false
                        {
                            if (rbList.Checked) // int list
                                IG.MinPath(i_first, i_second, out result, out weight);
                            else // rbMatrix.Checked // int matrix
                                IG.MinPath(i_first, i_second, out result, out weight);
                        }
                    }
                    else // rbString.Checked
                    {
                        first = frm.tbFirst.Text;
                        second = frm.tbSecond.Text;

                        if (cbUnmutable.Checked) // string unmutable
                            SG.MinPath(first, second, out result, out weight);
                        else // cbUnmutable.Checked = false
                        {
                            if (rbList.Checked) // string list
                                SG.MinPath(first, second, out result, out weight);
                            else // rbMatrix.Checked // string matrix
                                SG.MinPath(first, second, out result, out weight);
                        }
                    }
                    MessageBox.Show("Кратчайший путь равен " + weight.ToString() + '\n' + "Веса дуг, через которые проходит кратчайший путь:\n" + result);
                }
                catch (GraphException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void allPathes_Click(object sender, EventArgs e)
        {
            FormInputQuery frm = new FormInputQuery("Введите значения вершин.", false);
            frm.ShowDialog();
            int i_first = 0, i_second = 0;
            string first = "", second = "", result = "";
            if (frm.ok)
            {
                try
                {
                    if (rbInt.Checked)
                    {
                        if (!(Int32.TryParse(frm.tbFirst.Text, out i_first) && Int32.TryParse(frm.tbSecond.Text, out i_second)))
                            throw new GraphException("Неверный тип элемента. Вершины графа имеют тип " + i_first.GetType().ToString());

                        if (cbUnmutable.Checked) // int unmutable
                            IG.AllPathes(i_first, i_second, out result);
                        else // cbUnmutable.Checked = false
                        {
                            if (rbList.Checked) // int list
                                IG.AllPathes(i_first, i_second, out result);
                            else // rbMatrix.Checked // int matrix
                                IG.AllPathes(i_first, i_second, out result);
                        }
                    }
                    else // rbString.Checked
                    {
                        first = frm.tbFirst.Text;
                        second = frm.tbSecond.Text;

                        if (cbUnmutable.Checked) // string unmutable
                            SG.AllPathes(first, second, out result);
                        else // cbUnmutable.Checked = false
                        {
                            if (rbList.Checked) // string list
                                SG.AllPathes(first, second, out result);
                            else // rbMatrix.Checked // string matrix
                                SG.AllPathes(first, second, out result);
                        }
                    }
                    MessageBox.Show("Всевозможные пути: \n" + result);
                }
                catch (GraphException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void depthTraversal_Click(object sender, EventArgs e)
        {
            try
            {
                string str;
                if (rbInt.Checked)
                    IG.DoDepthTraversal(out str);
                else
                    SG.DoDepthTraversal(out str);
                MessageBox.Show("Порядок обхода вершин при обходе в глубину:\n" + str);
            }
            catch (GraphException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void breadthTraversal_Click(object sender, EventArgs e)
        {
            try
            {
                string str;
                if (rbInt.Checked)
                    IG.DoBreadthTraversal(out str);
                else
                    SG.DoBreadthTraversal(out str);
                MessageBox.Show("Порядок обхода вершин при обходе в ширину:\n" + str);
            }
            catch (GraphException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void clearGraph_Click(object sender, EventArgs e)
        {
            if (IG != null)
                IG.Clear();
            else
                if (SG != null)
                    SG.Clear();
            Grid.Rows.Clear();
            delEdge.Enabled = false;
            clearGraph.Enabled = false;
            depthTraversal.Enabled = false;
            breadthTraversal.Enabled = false;
            shortestWay.Enabled = false;
            allPathes.Enabled = false;

            findNode.Enabled = false;
            countOfNodes.Enabled = false;
            checkNodes.Enabled = false;
            getNodesInOrder.Enabled = false;
        }
        private void rbInt_CheckedChanged(object sender, EventArgs e)
        {
            IG = null;
            SG = null;
            Grid.Rows.Clear();
            delEdge.Enabled = false;
            clearGraph.Enabled = false;
            depthTraversal.Enabled = false;
            breadthTraversal.Enabled = false;
            shortestWay.Enabled = false;
            allPathes.Enabled = false;

            findNode.Enabled = false;
            countOfNodes.Enabled = false;
            checkNodes.Enabled = false;
            getNodesInOrder.Enabled = false;
        }
        private void rbString_CheckedChanged(object sender, EventArgs e)
        {
            IG = null;
            SG = null;
            Grid.Rows.Clear();
            delEdge.Enabled = false;
            clearGraph.Enabled = false;
            depthTraversal.Enabled = false;
            breadthTraversal.Enabled = false;
            shortestWay.Enabled = false;
            allPathes.Enabled = false;

            findNode.Enabled = false;
            countOfNodes.Enabled = false;
            checkNodes.Enabled = false;
            getNodesInOrder.Enabled = false;
        }
        private void rbList_CheckedChanged(object sender, EventArgs e)
        {
            IG = null;
            SG = null;
            Grid.Rows.Clear();
            delEdge.Enabled = false;
            clearGraph.Enabled = false;
            depthTraversal.Enabled = false;
            breadthTraversal.Enabled = false;
            shortestWay.Enabled = false;
            allPathes.Enabled = false;

            findNode.Enabled = false;
            countOfNodes.Enabled = false;
            checkNodes.Enabled = false;
            getNodesInOrder.Enabled = false;
        }
        private void rbMatrix_CheckedChanged(object sender, EventArgs e)
        {
            IG = null;
            SG = null;
            Grid.Rows.Clear();
            delEdge.Enabled = false;
            clearGraph.Enabled = false;
            depthTraversal.Enabled = false;
            breadthTraversal.Enabled = false;
            shortestWay.Enabled = false;
            allPathes.Enabled = false;

            findNode.Enabled = false;
            countOfNodes.Enabled = false;
            checkNodes.Enabled = false;
            getNodesInOrder.Enabled = false;
        }

        private void findNode_Click(object sender, EventArgs e)
        {
            if (rbInt.Checked)
            {
                CheckDelegate<int> del = new CheckDelegate<int>(CheckInt);
                if (GraphUtils.Exists(IG, del))
                    MessageBox.Show("В графе существуют вершины с четными значениями.");
                else
                    MessageBox.Show("В графе не существуют вершины с четными значениями.");
            }
            else
                {
                    CheckDelegate<string> del = new CheckDelegate<string>(CheckStr);
                    if (GraphUtils.Exists(SG, del))
                        MessageBox.Show("В графе существуют вершины с четной длиной.");
                    else
                        MessageBox.Show("В графе не существуют вершины с четной длиной.");
                }
        }
        private void countOfNodes_Click(object sender, EventArgs e)
        {
            if (rbInt.Checked)
            {
                CheckDelegate<int> del = new CheckDelegate<int>(CheckInt);
                MessageBox.Show("Количество вершин с четными значениями равно " + GraphUtils.FindAll(IG, del));
            }
            else
            {
                CheckDelegate<string> del = new CheckDelegate<string>(CheckStr);
                MessageBox.Show("Количество вершин с четной длиной равно " + GraphUtils.FindAll(SG, del));
            }
        }
        private void checkNodes_Click(object sender, EventArgs e)
        {
            if (rbInt.Checked)
            {
                CheckDelegate<int> del = new CheckDelegate<int>(CheckInt);
                if (GraphUtils.CheckForAll(IG, del))
                    MessageBox.Show("Все вершины графа четные.");
                else
                    MessageBox.Show("Не все вершины графа четные.");
            }
            else
            {
                CheckDelegate<string> del = new CheckDelegate<string>(CheckStr);
                if (GraphUtils.CheckForAll(SG, del))
                    MessageBox.Show("Все вершины графа имеют четную длину.");
                else
                    MessageBox.Show("Не все вершины графа имеют четную длину.");
            }
        }
        private void getNodesInOrder_Click(object sender, EventArgs e)
        {
            if (rbInt.Checked)
            {
                CompareDelegate<int> del = new CompareDelegate<int>(CompareInt);
                MessageBox.Show("Вершины графа в порядке возрастания:\n" + GraphUtils.GetAllNodes(IG, del));
            }
            else
            {
                CompareDelegate<string> del = new CompareDelegate<string>(CompareStr);
                MessageBox.Show("Вершины графа в порядке возрастания:\n" + GraphUtils.GetAllNodes(SG, del));
            }
        }

        bool CheckInt(int value)
        {
            return (value % 2 == 0);
        }
        bool CheckStr(string value)
        {
            return (value.Length % 2 == 0);
        }
        int CompareInt(int elem1, int elem2)
        {
            return elem1.CompareTo(elem2);
        }
        int CompareStr(string elem1, string elem2)
        {
            return String.Compare(elem1, elem2);
        }
    }
}
