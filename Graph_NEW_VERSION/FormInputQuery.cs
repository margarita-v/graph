﻿using System;
using System.Windows.Forms;

namespace Graph_NEW_VERSION
{
    public partial class FormInputQuery : Form
    {
        public bool ok = false;
        public FormInputQuery(string query, bool for_add)
        {
            InitializeComponent();
            labelQuery.Text = query;
            labelWeight.Visible = for_add;
            tbWeight.Visible = for_add;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ok = true;
            Close();
        }
    }
}
