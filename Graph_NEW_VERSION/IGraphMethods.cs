﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace Graph_NEW_VERSION
{
    public interface IGraphMethods<V, E> : IEnumerable<V>
    {
        void DoDepthTraversal(out string res);
        void DoBreadthTraversal(out string res);

        void MinPath(V first, V second, out string res, out int weight);
        void AllPathes(V first, V second, out string res);

        void GetMatrix(DataGridView grid);

        void DoAddition(V first, V second, E weight, DataGridView grid);
    }
}
