﻿namespace Graph_NEW_VERSION
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.редактированиеГрафаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addEdge = new System.Windows.Forms.ToolStripMenuItem();
            this.delEdge = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.clearGraph = new System.Windows.Forms.ToolStripMenuItem();
            this.поискВГрафеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shortestWay = new System.Windows.Forms.ToolStripMenuItem();
            this.allPathes = new System.Windows.Forms.ToolStripMenuItem();
            this.findNode = new System.Windows.Forms.ToolStripMenuItem();
            this.countOfNodes = new System.Windows.Forms.ToolStripMenuItem();
            this.работаСГрафомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.depthTraversal = new System.Windows.Forms.ToolStripMenuItem();
            this.breadthTraversal = new System.Windows.Forms.ToolStripMenuItem();
            this.checkNodes = new System.Windows.Forms.ToolStripMenuItem();
            this.getNodesInOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.Grid = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbList = new System.Windows.Forms.RadioButton();
            this.rbMatrix = new System.Windows.Forms.RadioButton();
            this.rbInt = new System.Windows.Forms.RadioButton();
            this.rbString = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbUnmutable = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.редактированиеГрафаToolStripMenuItem,
            this.поискВГрафеToolStripMenuItem,
            this.работаСГрафомToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(730, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // редактированиеГрафаToolStripMenuItem
            // 
            this.редактированиеГрафаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addEdge,
            this.delEdge,
            this.toolStripMenuItem1,
            this.clearGraph});
            this.редактированиеГрафаToolStripMenuItem.Name = "редактированиеГрафаToolStripMenuItem";
            this.редактированиеГрафаToolStripMenuItem.Size = new System.Drawing.Size(144, 20);
            this.редактированиеГрафаToolStripMenuItem.Text = "Редактирование графа";
            // 
            // addEdge
            // 
            this.addEdge.Name = "addEdge";
            this.addEdge.Size = new System.Drawing.Size(163, 22);
            this.addEdge.Text = "Добавить ребро";
            this.addEdge.Click += new System.EventHandler(this.addEdge_Click);
            // 
            // delEdge
            // 
            this.delEdge.Enabled = false;
            this.delEdge.Name = "delEdge";
            this.delEdge.Size = new System.Drawing.Size(163, 22);
            this.delEdge.Text = "Удалить ребро";
            this.delEdge.Click += new System.EventHandler(this.delEdge_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(160, 6);
            // 
            // clearGraph
            // 
            this.clearGraph.Enabled = false;
            this.clearGraph.Name = "clearGraph";
            this.clearGraph.Size = new System.Drawing.Size(163, 22);
            this.clearGraph.Text = "Очистить граф";
            this.clearGraph.Click += new System.EventHandler(this.clearGraph_Click);
            // 
            // поискВГрафеToolStripMenuItem
            // 
            this.поискВГрафеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shortestWay,
            this.allPathes,
            this.findNode,
            this.countOfNodes});
            this.поискВГрафеToolStripMenuItem.Name = "поискВГрафеToolStripMenuItem";
            this.поискВГрафеToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.поискВГрафеToolStripMenuItem.Text = "Поиск в графе";
            // 
            // shortestWay
            // 
            this.shortestWay.Enabled = false;
            this.shortestWay.Name = "shortestWay";
            this.shortestWay.Size = new System.Drawing.Size(346, 22);
            this.shortestWay.Text = "Поиск кратчайшего пути";
            this.shortestWay.Click += new System.EventHandler(this.shortestWay_Click);
            // 
            // allPathes
            // 
            this.allPathes.Enabled = false;
            this.allPathes.Name = "allPathes";
            this.allPathes.Size = new System.Drawing.Size(346, 22);
            this.allPathes.Text = "Поиск всех путей";
            this.allPathes.Click += new System.EventHandler(this.allPathes_Click);
            // 
            // findNode
            // 
            this.findNode.Enabled = false;
            this.findNode.Name = "findNode";
            this.findNode.Size = new System.Drawing.Size(346, 22);
            this.findNode.Text = "Поиск вершин, удовлетворяющих условию";
            this.findNode.Click += new System.EventHandler(this.findNode_Click);
            // 
            // countOfNodes
            // 
            this.countOfNodes.Enabled = false;
            this.countOfNodes.Name = "countOfNodes";
            this.countOfNodes.Size = new System.Drawing.Size(346, 22);
            this.countOfNodes.Text = "Количество вершин, удовлетворяющих условию";
            this.countOfNodes.Click += new System.EventHandler(this.countOfNodes_Click);
            // 
            // работаСГрафомToolStripMenuItem
            // 
            this.работаСГрафомToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.depthTraversal,
            this.breadthTraversal,
            this.checkNodes,
            this.getNodesInOrder});
            this.работаСГрафомToolStripMenuItem.Name = "работаСГрафомToolStripMenuItem";
            this.работаСГрафомToolStripMenuItem.Size = new System.Drawing.Size(112, 20);
            this.работаСГрафомToolStripMenuItem.Text = "Работа с графом";
            // 
            // depthTraversal
            // 
            this.depthTraversal.Enabled = false;
            this.depthTraversal.Name = "depthTraversal";
            this.depthTraversal.Size = new System.Drawing.Size(357, 22);
            this.depthTraversal.Text = "Обход в глубину";
            this.depthTraversal.Click += new System.EventHandler(this.depthTraversal_Click);
            // 
            // breadthTraversal
            // 
            this.breadthTraversal.Enabled = false;
            this.breadthTraversal.Name = "breadthTraversal";
            this.breadthTraversal.Size = new System.Drawing.Size(357, 22);
            this.breadthTraversal.Text = "Обход в ширину";
            this.breadthTraversal.Click += new System.EventHandler(this.breadthTraversal_Click);
            // 
            // checkNodes
            // 
            this.checkNodes.Enabled = false;
            this.checkNodes.Name = "checkNodes";
            this.checkNodes.Size = new System.Drawing.Size(357, 22);
            this.checkNodes.Text = "Проверка условия";
            this.checkNodes.Click += new System.EventHandler(this.checkNodes_Click);
            // 
            // getNodesInOrder
            // 
            this.getNodesInOrder.Enabled = false;
            this.getNodesInOrder.Name = "getNodesInOrder";
            this.getNodesInOrder.Size = new System.Drawing.Size(357, 22);
            this.getNodesInOrder.Text = "Получить значения вершин в порядке возрастания";
            this.getNodesInOrder.Click += new System.EventHandler(this.getNodesInOrder_Click);
            // 
            // Grid
            // 
            this.Grid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Grid.Location = new System.Drawing.Point(0, 103);
            this.Grid.Name = "Grid";
            this.Grid.ReadOnly = true;
            this.Grid.Size = new System.Drawing.Size(730, 374);
            this.Grid.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Выбрать тип элементов";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Представление графа";
            // 
            // rbList
            // 
            this.rbList.AutoSize = true;
            this.rbList.Checked = true;
            this.rbList.Location = new System.Drawing.Point(6, 19);
            this.rbList.Name = "rbList";
            this.rbList.Size = new System.Drawing.Size(145, 17);
            this.rbList.TabIndex = 6;
            this.rbList.TabStop = true;
            this.rbList.Text = "Список вершин и ребер";
            this.rbList.UseVisualStyleBackColor = true;
            this.rbList.CheckedChanged += new System.EventHandler(this.rbList_CheckedChanged);
            // 
            // rbMatrix
            // 
            this.rbMatrix.AutoSize = true;
            this.rbMatrix.Location = new System.Drawing.Point(6, 40);
            this.rbMatrix.Name = "rbMatrix";
            this.rbMatrix.Size = new System.Drawing.Size(129, 17);
            this.rbMatrix.TabIndex = 7;
            this.rbMatrix.Text = "Матрица смежности";
            this.rbMatrix.UseVisualStyleBackColor = true;
            this.rbMatrix.CheckedChanged += new System.EventHandler(this.rbMatrix_CheckedChanged);
            // 
            // rbInt
            // 
            this.rbInt.AutoSize = true;
            this.rbInt.Checked = true;
            this.rbInt.Location = new System.Drawing.Point(9, 19);
            this.rbInt.Name = "rbInt";
            this.rbInt.Size = new System.Drawing.Size(36, 17);
            this.rbInt.TabIndex = 4;
            this.rbInt.TabStop = true;
            this.rbInt.Text = "int";
            this.rbInt.UseVisualStyleBackColor = true;
            // 
            // rbString
            // 
            this.rbString.AutoSize = true;
            this.rbString.Location = new System.Drawing.Point(9, 40);
            this.rbString.Name = "rbString";
            this.rbString.Size = new System.Drawing.Size(50, 17);
            this.rbString.TabIndex = 5;
            this.rbString.Text = "string";
            this.rbString.UseVisualStyleBackColor = true;
            this.rbString.CheckedChanged += new System.EventHandler(this.rbString_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rbInt);
            this.groupBox1.Controls.Add(this.rbString);
            this.groupBox1.Location = new System.Drawing.Point(388, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(146, 65);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.rbList);
            this.groupBox2.Controls.Add(this.rbMatrix);
            this.groupBox2.Location = new System.Drawing.Point(540, 32);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(178, 66);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            // 
            // cbUnmutable
            // 
            this.cbUnmutable.AutoSize = true;
            this.cbUnmutable.Enabled = false;
            this.cbUnmutable.Location = new System.Drawing.Point(12, 72);
            this.cbUnmutable.Name = "cbUnmutable";
            this.cbUnmutable.Size = new System.Drawing.Size(133, 17);
            this.cbUnmutable.TabIndex = 11;
            this.cbUnmutable.TabStop = true;
            this.cbUnmutable.Text = "Неизменяемый граф";
            this.cbUnmutable.UseVisualStyleBackColor = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 477);
            this.Controls.Add(this.cbUnmutable);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Grid);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Неориентированный граф";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem редактированиеГрафаToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem addEdge;
        public System.Windows.Forms.ToolStripMenuItem delEdge;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem clearGraph;
        private System.Windows.Forms.ToolStripMenuItem поискВГрафеToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem shortestWay;
        public System.Windows.Forms.ToolStripMenuItem allPathes;
        private System.Windows.Forms.ToolStripMenuItem работаСГрафомToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem depthTraversal;
        public System.Windows.Forms.ToolStripMenuItem breadthTraversal;
        public System.Windows.Forms.DataGridView Grid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.RadioButton rbList;
        public System.Windows.Forms.RadioButton rbMatrix;
        public System.Windows.Forms.RadioButton rbInt;
        public System.Windows.Forms.RadioButton rbString;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.ToolStripMenuItem findNode;
        public System.Windows.Forms.ToolStripMenuItem countOfNodes;
        public System.Windows.Forms.ToolStripMenuItem checkNodes;
        public System.Windows.Forms.ToolStripMenuItem getNodesInOrder;
        public System.Windows.Forms.RadioButton cbUnmutable;
    }
}

