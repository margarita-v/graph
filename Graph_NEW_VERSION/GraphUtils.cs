﻿using System;

namespace Graph_NEW_VERSION
{
    public delegate bool CheckDelegate<T>(T source);
    public delegate int CompareDelegate<T>(T elem1, T elem2);
    public delegate IGraph<V, E> GraphConstructorDelegate<V, E>()
        where E : IComparable
        where V : IComparable;

    public class GraphUtils
    {
        // существует ли вершина с данным значением
        public static bool Exists<V, E>(IGraph<V, E> graph, CheckDelegate<V> check)
            where E : IComparable
            where V : IComparable
        {
            foreach (var node in graph.GetListOfNodes)
            {
                if (check(node))
                    return true;
            }
            return false;
        }

        // найти количество всех вершин с данным значением
        public static int FindAll<V, E>(IGraph<V, E> graph, CheckDelegate<V> check)
            where E : IComparable
            where V : IComparable
        {
            int res = 0;
            foreach (var node in graph.GetListOfNodes)
            {
                if (check(node))
                    res++;
            }
            return res;
        }

        // проверить, удовлетворяют ли все вершины графа заданному условию
        public static bool CheckForAll<V, E>(IGraph<V, E> graph, CheckDelegate<V> check)
        {
            bool ok = true;
            foreach (var node in graph.GetListOfNodes)
            {
                ok = ok && check(node);
            }
            return ok;
        }

        // возвращает значения вершин графа в порядке возрастания
        public static string GetAllNodes<V, E>(IGraph<V, E> graph, CompareDelegate<V> cmp)
        {
            string res = "";
            var list = graph.GetListOfNodes;
            for (int i = 0; i < list.Count; i++ )
                for (int j = 0; j < list.Count; j++ )
                    if (cmp(list[i], list[j]) < 0)
                    {
                        V tmp = list[i];
                        list[i] = list[j];
                        list[j] = tmp;
                    }
            for (int i = 0; i < list.Count; i++)
                res += list[i].ToString() + '\n';
            return res;
        }

        public static GraphConstructorDelegate<V, E> GraphConstructor<V, E>()
            where E : IComparable
            where V : IComparable
        {
            return new GraphConstructorDelegate<V, E>(Activator.CreateInstance<Graph<V, E>>);
        }
        public static GraphConstructorDelegate<V, E> GraphMatrixConstructor<V, E>()
            where E : IComparable
            where V : IComparable
        {
            return new GraphConstructorDelegate<V, E>(Activator.CreateInstance<GraphMatrix<V, E>>);
        }

        /*
        public static IGraph<V, E> GetAllNodes<V, E>(IGraph<V, E> graph, CompareDelegate<V> cmp, GraphConstructorDelegate<V, E> constr)
        {
            IGraph<V, E> res = constr();

            var list = graph.GetListOfNodes;
            for (int i = 0; i < list.Count; i++)
                for (int j = 0; j < list.Count; j++)
                    if (cmp(list[i], list[j]) < 0)
                    {
                        V tmp = list[i];
                        list[i] = list[j];
                        list[j] = tmp;
                    }
            for (int i = 0; i < list.Count; i++)
                res.AddNode(list[i]);
            return res;
        }*/
    }
}
