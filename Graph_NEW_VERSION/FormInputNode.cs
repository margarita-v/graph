﻿using System;
using System.Windows.Forms;

namespace Graph_NEW_VERSION
{
    public partial class FormInputNode : Form
    {
        public bool ok = false;
        public FormInputNode(string query)
        {
            InitializeComponent();
            labelText.Text = query;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ok = true;
            Close();
        }
    }
}
