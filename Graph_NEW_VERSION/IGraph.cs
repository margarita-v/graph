﻿namespace Graph_NEW_VERSION
{
    public interface IGraph<V, E> : IGraphMethods<V, E>
    {
        void AddNode(V info);
        void AddEdge(V StartNode, V EndNode, E weight);

        void Clear();

        bool ContainsNode(V info);
        bool ContainsEdge(V StartNode, V EndNode);

        void RemoveNode(V info);
        void RemoveEdge(V StartNode, V EndNode);
        void DeleteSingleNodes();

        int CountOfNodes { get; }
        int CountOfEdges { get; }

        Elements<V> GetListOfNodes { get; }
    }
}
