﻿using System;

namespace Graph_NEW_VERSION
{
    public class GraphException : ApplicationException
    {
        public GraphException() : base() { }
        public GraphException(string str) : base(str) { }
        public override string ToString()
        {
            return Message;
        }
    }
}
