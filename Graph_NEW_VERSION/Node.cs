﻿using System;

namespace Graph_NEW_VERSION
{
    public class Node<V, E>
    {
        V info;                        // информация, хранящаяся в вершине графа
        Elements<Edge<V, E>> links;

        public Node(V pInfo = default(V))
        {
            info = pInfo;
            links = new Elements<Edge<V, E>>();
        }
        public V Info
        {
            get { return info; }
            set { info = value; }
        }
        public int LinksCount
        {
            get { return links.Count; }
            set { links.Count = value; }
        }
        public Elements<Edge<V, E>> Links
        {
            get { return links; }
            set { links = value; }
        }

        bool is_marked = false;
        E mark = default(E);

        public E Mark
        {
            get { return mark; }
            set { mark = value; }
        }
        public bool Is_Marked
        {
            get { return is_marked; }
            set { is_marked = value; }
        }

        public override string ToString() { return Info.ToString(); }
        public override bool Equals(object O)
        {
            Node<V, E> N = (Node<V, E>)O;
            if (N == null) throw new ArgumentException("Type " + O.GetType() + " cannot be compared with type " + GetType() + " !");
            return info.Equals(N.Info);
        }
        public object Clone()
        {
            Node<V, E> N = new Node<V, E>(Info);
            return N;
        }
        public override int GetHashCode() { return Info.GetHashCode(); }
    }
}
