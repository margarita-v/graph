﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Graph_NEW_VERSION
{
    public class Graph<V, E> : IGraph<V, E>
        where E : IComparable
        where V : IComparable
    {
        Elements<Node<V, E>> listOfNodes;  // список вершин графа
        Elements<Edge<V, E>> listOfEdges;  // список ребер графа

        public Graph()
        {
            listOfNodes = new Elements<Node<V, E>>();
            listOfEdges = new Elements<Edge<V, E>>();
        }
        public Elements<Node<V, E>> ListOfNodes { get { return listOfNodes; } }
        public Elements<Edge<V, E>> ListOfEdges { get { return listOfEdges; } }

        public Node<V, E> FindNode(V info)
        {
            foreach (Node<V, E> node in listOfNodes)
            {
                if (node.Info.Equals(info))
                    return node;
            }
            return null;
        }
        public Edge<V, E> FindEdge(V first, V second)
        {
            foreach (Edge<V, E> edge in listOfEdges)
            {
                if (edge.StartNode.Info.Equals(first) && edge.EndNode.Info.Equals(second) || edge.StartNode.Info.Equals(second) && edge.EndNode.Info.Equals(first))
                    return edge;
            }
            return null;
        }

        // РЕАЛИЗАЦИЯ МЕТОДОВ ИНТЕРФЕЙСА

        public void AddNode(V info)
        {
            Node<V, E> NewNode = new Node<V, E>(info);
            if (listOfNodes.Contains(NewNode))
                throw new GraphException("Вершина " + info + " уже есть в графе.");
            listOfNodes.Add(NewNode);
        }
        public void AddEdge(V StartNode, V EndNode, E weight)
        {
            if (StartNode.Equals(default(V)) || EndNode.Equals(default(V)))
                throw new GraphException("Значения начальной и конечной вершин ребра не должны быть равны " + default(V));

            Edge<V, E> NewEdge = new Edge<V, E>(FindNode(StartNode), FindNode(EndNode));
            NewEdge.Weight = weight;
            if (listOfEdges.Contains(NewEdge))
                throw new GraphException("Такое ребро уже есть в графе.");
            if (!listOfNodes.Contains(NewEdge.StartNode) || !listOfNodes.Contains(NewEdge.EndNode))
                throw new GraphException("Cannot add an arc if one of its extremity nodes does not belong to the graph.");
            listOfEdges.Add(NewEdge);
            NewEdge.StartNode.Links.Add(NewEdge);
            NewEdge.EndNode.Links.Add(NewEdge);
        }

        public void Clear()
        {
            listOfNodes.Clear();
            listOfEdges.Clear();
        }

        public bool ContainsNode(V info) 
        {
            foreach (Node<V, E> node in listOfNodes)
            {
                if (node.Info.Equals(info))
                    return true;
            }
            return false;
        }
        public bool ContainsEdge(V StartNode, V EndNode)
        {
            foreach (Edge<V, E> edge in listOfEdges)
            {
                if (edge.StartNode.Info.Equals(StartNode) && edge.EndNode.Info.Equals(EndNode) || edge.StartNode.Info.Equals(EndNode) && edge.EndNode.Info.Equals(StartNode))
                    return true;
            }
            return false;
        }

        public void DeleteSingleNodes()
        {
            foreach (Node<V, E> node in listOfNodes)
            {
                if (node.Links.Count == 0)
                    listOfNodes.Remove(node);
            }
        }
        public void RemoveNode(V info)
        {
            if (!ContainsNode(info))
                throw new GraphException("Данная вершина отсутствует в графе.");
            Node<V, E> NodeToRemove = FindNode(info);
            if (NodeToRemove.Links != null)
                foreach (Edge<V, E> E in NodeToRemove.Links)
                {
                    listOfEdges.Remove(E);
                }
            listOfNodes.Remove(NodeToRemove);
        }
        public void RemoveEdge(V StartNode, V EndNode)
        {
            if (!ContainsEdge(StartNode, EndNode))
                throw new GraphException("Ребро между данными вершинами отсутствует в графе.");

            Edge<V, E> EdgeToRemove = FindEdge(StartNode, EndNode);
            foreach (Node<V, E> node in listOfNodes)
            {
                if (node.Links.Contains(EdgeToRemove))
                    node.Links.Remove(EdgeToRemove);
            }
            listOfEdges.Remove(EdgeToRemove);
            DeleteSingleNodes();
        }

        public int CountOfNodes 
        {
            get { return listOfNodes.Count; }
        }
        public int CountOfEdges 
        {
            get { return listOfEdges.Count; }
        }

        public Elements<V> GetListOfNodes 
        { 
            get
            {
                Elements<V> res = new Elements<V>();
                foreach (Node<V, E> node in listOfNodes)
                    res.Add(node.Info);
                return res;
            }
        }

        public IEnumerator<V> GetEnumerator()
        {
            foreach (Node<V, E> node in listOfNodes)
                yield return node.Info;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        private string items = "";
        void DepthTraversal(Node<V, E> node)
        {
            node.Is_Marked = true;
            items = items + node.Info + '\n';
            foreach (Edge<V, E> edge in node.Links)
            {
                if (edge.StartNode.Equals(node) && !edge.EndNode.Is_Marked)
                    DepthTraversal(edge.EndNode);
                else
                    if (edge.EndNode.Equals(node) && !edge.StartNode.Is_Marked)
                        DepthTraversal(edge.StartNode);
            }
        }
        public void DoDepthTraversal(out string res)
        {
            if (listOfEdges.Count > 0 && listOfNodes.Count > 0)
            {
                DepthTraversal(listOfNodes[0]);
                int count = 0;
                foreach (Node<V, E> node in listOfNodes)
                {
                    if (node.Is_Marked)
                        count++;
                    node.Is_Marked = false;
                }
                res = items;
                items = "";
                if (count < listOfNodes.Count)
                    throw new GraphException(count.ToString());
            }
            else
                throw new GraphException("Граф пуст.");
        }

        void BreadthTraversal(Node<V, E> node)
        {
            Node<V, E>[] q = new Node<V, E>[listOfNodes.Count + 1];
            int p_write = 1, p_read = 0;
            q[p_write] = node;
            node.Is_Marked = true;
            while (p_read < p_write)
            {
                p_read++;
                node = q[p_read];
                items = items + node.Info.ToString() + '\n';
                foreach (Edge<V, E> edge in node.Links)
                {
                    if (edge.StartNode.Equals(node) && !edge.EndNode.Is_Marked)
                    {
                        p_write++;
                        q[p_write] = edge.EndNode;
                        edge.EndNode.Is_Marked = true;
                    }
                    else
                        if (edge.EndNode.Equals(node) && !edge.StartNode.Is_Marked)
                        {
                            p_write++;
                            q[p_write] = edge.StartNode;
                            edge.StartNode.Is_Marked = true;
                        }
                }
            }
        }
        public void DoBreadthTraversal(out string res)
        {
            if (listOfEdges.Count > 0 && listOfNodes.Count > 0)
            {
                BreadthTraversal(listOfNodes[0]);
                int count = 0;
                foreach (Node<V, E> node in listOfNodes)
                {
                    if (node.Is_Marked)
                        count++;
                    node.Is_Marked = false;
                }
                res = items;
                items = "";
                if (count < listOfNodes.Count)
                    throw new GraphException(count.ToString());
            }
            else
                throw new GraphException("Граф пуст.");
        }

        Elements<string> ways;
        Elements<int> visited;
        string way = "";
        void TryGo(Node<V, E> node, Node<V, E> end, Elements<int> visited)
        {
            if (node == end)
                ways.Add(way);
            else
                foreach (Edge<V, E> edge in node.Links)
                {
                    if (edge.StartNode.Equals(node) && !visited.Contains(listOfNodes.IndexOf(edge.EndNode)))// || (edge.EndNode.Equals(_node) && !edge.StartNode.Const_Mark))
                    {
                        string w = way;
                        way = way + edge.Weight + ' ';
                        visited.Add(listOfNodes.IndexOf(edge.EndNode));
                        TryGo(edge.EndNode, end, visited);
                        visited.Remove(listOfNodes.IndexOf(edge.EndNode));
                        way = w;
                    }
                    else
                        if (edge.EndNode.Equals(node) && !visited.Contains(listOfNodes.IndexOf(edge.StartNode)))
                        {
                            string w = way;
                            way = way + edge.Weight + ' ';
                            visited.Add(listOfNodes.IndexOf(edge.EndNode));
                            TryGo(edge.StartNode, end, visited);
                            visited.Remove(listOfNodes.IndexOf(edge.EndNode));
                            way = w;
                        }
                }
        }

        void TryGo2(Node<V, E> node, Node<V, E> end, Elements<int> visited)
        {
            if (node == end)
                ways.Add(way);
            else
                foreach (Edge<V, E> edge in node.Links)
                {
                    if (edge.StartNode.Equals(node) && !visited.Contains(listOfNodes.IndexOf(edge.EndNode)))// || (edge.EndNode.Equals(_node) && !edge.StartNode.Const_Mark))
                    {
                        string w = way;
                        way = way + edge.EndNode.Info.ToString() + ' ';
                        visited.Add(listOfNodes.IndexOf(edge.EndNode));
                        TryGo2(edge.EndNode, end, visited);
                        visited.Remove(listOfNodes.IndexOf(edge.EndNode));
                        way = w;
                    }
                    else
                        if (edge.EndNode.Equals(node) && !visited.Contains(listOfNodes.IndexOf(edge.StartNode)))
                        {
                            string w = way;
                            way = way + edge.StartNode.Info.ToString() + ' ';
                            visited.Add(listOfNodes.IndexOf(edge.EndNode));
                            TryGo2(edge.StartNode, end, visited);
                            visited.Remove(listOfNodes.IndexOf(edge.EndNode));
                            way = w;
                        }
                }
        }

        public void MinPath(V first, V second, out string res, out int weight)
        {
            ways = new Elements<string>();
            visited = new Elements<int>();
            res = "";
            int min_sum = Int32.MaxValue;
            weight = min_sum;
            Node<V, E> _first = FindNode(first);
            Node<V, E> _second = FindNode(second);

            visited.Add(listOfNodes.IndexOf(_first));
            TryGo(_first, _second, visited);
            foreach (string s in ways)
            {
                string[] m = s.Split(' ');
                int[] mas = new int[m.Length - 1];
                int sum = 0;
                for (int i = 0; i < m.Length - 1; i++)
                {
                    mas[i] = Int32.Parse(m[i]);
                    sum += mas[i];
                }
                if (sum < min_sum)
                {
                    min_sum = sum;
                    weight = min_sum;
                    res = s;
                }
                //res = res + s + '\n';
            }
            ways.Clear();
            visited.Clear();
            way = "";
        }

        public void AllPathes(V first, V second, out string res)
        {
            ways = new Elements<string>();
            visited = new Elements<int>();
            res = "";
            Node<V, E> _first = FindNode(first);
            Node<V, E> _second = FindNode(second);

            visited.Add(listOfNodes.IndexOf(_first));
            TryGo2(_first, _second, visited);

            foreach (string s in ways)
            {
                res = res + first.ToString() + ' ' + s + '\n';
            }
            ways.Clear();
            visited.Clear();
            way = "";
        }

        public void GetMatrix(DataGridView grid)
        {
            grid.Rows.Clear();
            if (ListOfNodes.Count > 0)
            {
                grid.RowCount = ListOfNodes.Count + 1;
                grid.ColumnCount = grid.RowCount;
                int i = 1;
                foreach (Node<V, E> Node in ListOfNodes)
                {
                    grid.Rows[0].Cells[i].Value = Node.ToString();
                    grid.Rows[i].Cells[0].Value = Node.ToString();
                    int j = 1;
                    if (ListOfEdges.Count > 0)
                        foreach (Edge<V, E> Edge in ListOfEdges)
                        {
                            if (Node.Links.Contains(Edge))
                                if (Edge.StartNode.Equals(Node))
                                    grid.Rows[ListOfNodes.IndexOf(Edge.EndNode) + 1].Cells[i].Value = Edge.Weight.ToString();
                                else
                                    grid.Rows[ListOfNodes.IndexOf(Edge.StartNode) + 1].Cells[i].Value = Edge.Weight.ToString();
                            j++;
                        }
                    i++;
                }
            }
        }

        public void DoAddition(V first, V second, E weight, DataGridView grid)
        {
            Node<V, E> first_node = FindNode(first);
            Node<V, E> second_node = FindNode(second);
            if (first_node == null)
                AddNode(first);
            if (second_node == null)
                AddNode(second);
            AddEdge(first, second, weight);
            GetMatrix(grid);
        }
    }
}
